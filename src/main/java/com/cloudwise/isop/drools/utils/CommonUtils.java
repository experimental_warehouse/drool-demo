package com.cloudwise.isop.drools.utils;

import com.cloudwise.isop.drools.module.CommonModule;
import com.cloudwise.isop.drools.module.MybatisModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author liaobaohuang
 */
@ThreadSafe
public class CommonUtils {
    private static Injector injector;

    private CommonUtils() {}

    public static Injector getInjector() {
        if (injector == null) {
            injector = initGuice();
        }
        return injector;
    }

    public static <T> T getInstance(Class<T> type) {
        return getInjector().getInstance(type);
    }

    protected static Injector initGuice() {
        return Guice.createInjector(new MybatisModule(), new CommonModule());
    }
}