package com.cloudwise.isop.drools.utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;

import java.net.URLDecoder;

@Slf4j
public class ConfigUtil {
    private static Config config;
    private static String conf;

    public static void setConf(String path){
        log.info("====================conf:{}", path);
        ConfigUtil.conf = URLDecoder.decode(path);
        log.info("====================decode conf:{}", ConfigUtil.conf);
    }

    public static Config getConf() {
        log.info("====================getConf conf:{}", ConfigUtil.conf);
        config = ConfigFactory.parseString(conf);
        return config;
    }
}
