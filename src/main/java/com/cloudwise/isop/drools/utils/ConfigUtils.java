package com.cloudwise.isop.drools.utils;

import com.cloudwise.isop.drools.StringRuleEntrance;
import com.google.common.base.Strings;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.net.URL;

public class ConfigUtils {
    private static Config config;

    public static Config getConfig(String path) {
        if (config == null) {
            synchronized (ConfigUtils.class) {
                if (config == null) {
                    config = Strings.isNullOrEmpty(path) ? ConfigFactory.load()
                            : ConfigFactory.parseFile(new File(path));
                }
            }
        }
        return config;
    }

    public static Config getConfig() {
        if(config == null){
            URL url = StringRuleEntrance.class.getClassLoader().getResource("conf/application.conf");
            config = ConfigFactory.parseFile(new File(url.getFile()));
        }

        return config;
    }

}
