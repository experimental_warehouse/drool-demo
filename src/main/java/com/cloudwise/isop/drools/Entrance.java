package com.cloudwise.isop.drools;

import com.cloudwise.isop.drools.listener.ComsumerListener;
import lombok.extern.slf4j.Slf4j;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.SubscriptionType;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.util.concurrent.TimeUnit;

/**
 * drools demo 入口文件， 使用文件规则
 * @Author jeffse.jiang
 */
@Slf4j
public class Entrance {

    public static void main(String[] args) throws PulsarClientException {
        KieServices kieServices = KieServices.get();
        KieContainer kContainer = kieServices.getKieClasspathContainer();
        // 这里的session 名称一定要和 xml 中名称一致
        KieSession kieSession1 = kContainer.newKieSession();
//        log.error(kieSession1.toString());
        PulsarClient pulsarClient = PulsarClient.builder().serviceUrl("pulsar://localhost:6650").build();
        pulsarClient.newConsumer().messageListener(new ComsumerListener(kieSession1))
                .topic("persistent://public/bdz001/byq.sds.zhibiao999")
                .subscriptionName("group1")
                .ackTimeout(10, TimeUnit.SECONDS)
                .subscriptionType(SubscriptionType.Shared)
                .subscribe();

//        kieSession1.fireAllRules();
    }
}
