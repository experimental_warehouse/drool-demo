package com.cloudwise.isop.drools.module;

import com.google.inject.AbstractModule;
import lombok.NoArgsConstructor;


@NoArgsConstructor
public class CommonModule extends AbstractModule {

	@Override
	protected void configure() {

	}

}
