package com.cloudwise.isop.drools.dao;

import com.cloudwise.isop.drools.pojo.AlertRulePO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author jeffse.jiang
 * @date 2020/06/22
 */
public interface AlertRuleDAO {
    @Select("select id, rule_id ruleId, name, rule, json_rule ruleJson from drools_rule where status = 2 and deleted_flag = 1")
    List<AlertRulePO> getList();
}
