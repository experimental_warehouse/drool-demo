package com.cloudwise.isop.drools.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lin.wang
 * @date 2020/09/23
 * 从MQ中接收到的数据  1、alert_job  2、模型计算
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalculateAbridgeAlertDataDTO {
    /**
     * 缺陷等级
     */
    private String d;

    /**
     * 告警规则id
     */
    private Integer rid;

    /**
     * 时间
     */
    private Long t;

    /**
     * 设备英文名
     */
    private String e;

    /**
     * 计算规则表id
     */
    private Long cid;

    /**
     * 数据标准英文名
     */
    private String dce;

    /**
     * 标准测点列表
     */
    private String c;

    /**
     * 站id
     */
    private String s;
}
