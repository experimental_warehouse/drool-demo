package com.cloudwise.isop.drools.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lin.wang
 * @date 2020/06/11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlertRulePO implements Serializable {
    private Long id;
    private String rule;
    private String ruleJson;
    private String name;
    private Integer ruleId;
}
