package com.cloudwise.isop.drools.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author lin.wang
 * @date 2020/06/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SourceDataDTO implements Serializable {
    /**
     * 变电站id
     */
    private String s;

    /**
     * 设备英文
     */
    private String e;

    /**
     * 测点
     */
    private String p;

    /**
     * 数值
     */
    private String v;

    /**
     * 采集时间戳
     */
    private Long t;

    /**
     * 标准测点数据集英文名
     */
    private String dataCriterionNameEn;

    /**
     * 标准测点英文名
     */
    private String criterionPointNameEn;

}
