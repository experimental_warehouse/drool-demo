package com.cloudwise.isop.drools.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Metric {
    /**
     * 标准测点英文名
     */
    private String criterion_point_name_en;
    /**
     * 设备英文名
     */
    private String equip_name_en;
    /**
     * 变电站英文名
     */
    private String station_id;
    /**
     * 时间
     */
    private String t;
    /**
     * 值
     */
    private String v;

    private Integer level;
}
