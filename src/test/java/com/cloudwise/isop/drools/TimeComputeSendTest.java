package com.cloudwise.isop.drools;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author lin.wang
 * @date 2020/07/15
 */
@Slf4j
public class TimeComputeSendTest {
    public static void main(String[] args) throws PulsarClientException, InterruptedException {
        PulsarClient client = PulsarClient.builder().serviceUrl("pulsar://localhost:6650").build();
        Producer<String> stringProducer = client.newProducer(Schema.STRING)
                //                .topic("POINT_DATA_TRANSFORMED_2_ALERT_RULE")
                .topic("persistent://public/bdz001/byq.sds.zhibiao999")
                //队列消息数量（特别注意，异步发送，超过最大队列则报 Producer send queue is full）
                .maxPendingMessages(5000)
                //send timeout
                .sendTimeout(10, TimeUnit.SECONDS)
                .create();

        while (true) {
            List<JSONObject> list = Lists.newArrayList();
            for (int i = 1; i <= 10; i++) {
                JSONObject resultData = new JSONObject();
                resultData.put("e", "equipNameEn");
                resultData.put("p", "zhibiao999");
                resultData.put("s", "bdz001");
                Double value = Math.random() * 30;
                resultData.put("v", String.valueOf(value));
                resultData.put("t", System.currentTimeMillis());
                list.add(resultData);
            }

            log.error("=========msg is:{}", JSONObject.toJSONString(list));
            stringProducer.sendAsync(JSONObject.toJSONString(list));
            Thread.sleep(58);
            list.clear();
        }
    }
}
