package com.cloudwise.isop.drools;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class PulsarTest {
    private final static int DEFAULT_PENDING_MESSAGES = 5_000;
    private final static int DEFAULT_SEND_TIMEOUT = 10;
    @Test
    public void send() throws PulsarClientException, InterruptedException {
        PulsarClient pulsarClient = PulsarClient.builder().serviceUrl("pulsar://localhost:6650").build();
        Producer<String> producer = pulsarClient.newProducer(Schema.STRING)
                .maxPendingMessages(DEFAULT_PENDING_MESSAGES)
                .blockIfQueueFull(true)
                .enableBatching(true)
                .blockIfQueueFull(true)
                .sendTimeout(DEFAULT_SEND_TIMEOUT, TimeUnit.SECONDS)
                .topic("bbb")
                .create();
        while(true){
            Double value = Math.random() * 30;
            producer.send(value+"");
            Thread.sleep(1000);
        }


    }
}
