package com.cloudwise.isop.drools.pojo;

import lombok.Data;

@Data
public class Message {
    private String type;
}
