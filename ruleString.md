### 规则样本描述
直接复制，规则转换后的样本，可直接放入数据库

```$xslt
package rules
import com.cloudwise.isop.drools.pojo.Metric
import com.cloudwise.isop.drools.pojo.SourceDataDTO
import com.cloudwise.isop.drools.utils.AlertDataUtils
import java.util.List;

rule "开关柜接地异常-正常"
    no-loop true
    when
        $li:List(size > 0)
        $Value:SourceDataDTO(dataCriterionNameEn == "sds" && criterionPointNameEn == "zhibiao999" && Double.parseDouble(v) > 10 && Double.parseDouble(v) <= 15) from $li
    then
        System.out.println("正常");
        AlertDataUtils.send(1213132132L, $Value, "NORMAL");
end
```