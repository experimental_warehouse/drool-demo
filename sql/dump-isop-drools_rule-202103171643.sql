-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: isop
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drools_rule`
--

DROP TABLE IF EXISTS `drools_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `drools_rule` (
  `id` bigint NOT NULL,
  `json_rule` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '原始规则',
  `rule` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '解析后的规则',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `status` tinyint NOT NULL DEFAULT '2' COMMENT '状态（0新建 1启用 2禁用）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `deleted_flag` tinyint NOT NULL DEFAULT '1' COMMENT '是否删除 1 未删除 2 删除',
  `rule_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `drools_rule_id_IDX` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drools_rule`
--

LOCK TABLES `drools_rule` WRITE;
/*!40000 ALTER TABLE `drools_rule` DISABLE KEYS */;
INSERT INTO `drools_rule` VALUES (1213132132,'{\"alertName\":\"开关柜接地异常\",\"alertMessageTemplate\":\"一号主变高温套管告警，告警级别${result}\",\"bigScreenAlertWindowContinueTime\":1,\"voiceBroadcastFlag\":1,\"disableAlertTime\":1000,\"callbackList\":[{\"url\":\"http://xxxxx\",\"name\":\"xxx\"}],\"noteUserList\":[{\"userId\":1,\"messageType\":[\"email\",\"phone\",\"note\",\"websocket\"]}],\"rule\":[{\"ruleType\":\"一级\",\"ruleExpression\":\"01告警线\",\"ruleItemList\":[{\"alertLineName\":\"01告警线\",\"dataCriterionNameEn\":\"sds\",\"criterionPointNameEn\":\"zhibiao999\",\"criterionPointNameCh\":\"测点1\",\"valueCycle\":5,\"valueCycleUnit\":\"dataNum\",\"valueType\":\"min\",\"repeat\":2,\"repeatUnit\":\"dataNum\",\"sumNum\":1,\"sumUnit\":\"dataNum\",\"computeEnable\":false,\"repeatEnable\":false,\"thresholdValueInterval\":[{\"operation\":\"GREATER_THAN_OR_EQUAL_TO\",\"threshold\":10},{\"operation\":\"LESS_THAN_OR_EQUAL_TO\",\"threshold\":20}]}]}]}','package rules\nimport com.cloudwise.isop.drools.pojo.Metric\nimport com.cloudwise.isop.drools.pojo.SourceDataDTO\nimport com.cloudwise.isop.drools.utils.AlertDataUtils\nimport java.util.List;\n\nrule \"开关柜接地异常-正常\"\n    no-loop true\n    when\n        $li:List(size > 0)\n        $Value:SourceDataDTO(dataCriterionNameEn == \"sds\" && criterionPointNameEn == \"zhibiao999\" && Double.parseDouble(v) > 10 && Double.parseDouble(v) <= 15) from $li\n    then\n        System.out.println(\"正常\");\n        AlertDataUtils.send(1213132132L, $Value, \"NORMAL\");\nend','开关柜接地异常',2,'2021-01-31 15:25:03','2021-01-31 15:25:03',1,111);
/*!40000 ALTER TABLE `drools_rule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-17 16:43:43
